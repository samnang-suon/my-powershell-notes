# My PowerShell Notes
## Where to download PowerShell Core
https://github.com/powershell/powershell

## PowerShell Command Syntax
[Verb-Noun] -[Parameter1] [ValueParameter1] -[Parameter2] [ValueParameter2] ... -[ParameterN]

![PowerShellCommandSyntax](images/PowerShellCommandSyntax.png)

## PowerShell Variables

![PowerShellVariable](images/PowerShellVariable.png)

https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.core/about/about_variables?view=powershell-7.1#types-of-variables

## Check your PowerShell version
* Get-Host | Select-Object Version

source: https://winaero.com/find-powershell-version-windows/

OR

* $PSVersionTable.PSVersion

source: https://docs.microsoft.com/en-us/powershell/azure/install-az-ps?view=azps-6.0.0

## Some Useful Commands to Know
| Command | Description |
|---------|-------------|
get-verb &#124; more |
get-verb -group [CommandGroupName] |
get-help |
get-help [PowerShellCommand] | Print the command's help
update-help | To download and install Help files
[PowerShellCommand] &#124; Format-List | Format the output
get-alias | Show list of existing alias for each command
get-alias -definition *[VerbOrNoun]* | Filter alias for what you are looking for
get-command | List all commands
get-command -verb get -noun *dns* |
get-command -name *fire* -commandType function |
get-member | Gets the properties and methods of objects
get-service &#124; get-member | get all properties of get-service object

## Advance Functions
| Function Object | Description | URL |
|-----------------|-------------|-----|
Sort-Object | Sorts objects by property values | https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.utility/sort-object?view=powershell-7.1
Where-Object | Selects objects from a collection based on their property values | https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.core/where-object?view=powershell-7.1
Select-Object | Selects objects or object properties | https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.utility/select-object?view=powershell-7.1

## Working with modules
| Function Object | Description | URL |
|-----------------|-------------|-----|
Get-Module -ListAvailable |
import-module [ModuleName] |

## Install Azure PowerShell Module

    Install-Module -Name Az -Scope CurrentUser -Repository PSGallery -Force

source: https://docs.microsoft.com/en-us/powershell/azure/install-az-ps?view=azps-6.0.0#installation

## How to read arguments in PowerShell script
See: https://stackoverflow.com/questions/2157554/how-to-handle-command-line-arguments-in-powershell

## Learning PowerShell Resources
* https://docs.microsoft.com/en-us/powershell/scripting/learn/ps101/00-introduction?view=powershell-7.1
* https://github.com/PowerShell/PowerShell/tree/master/docs/learning-powershell
* https://www.varonis.com/blog/windows-powershell-tutorials/
* https://searchwindowsserver.techtarget.com/Comprehensive-PowerShell-guide-for-new-and-seasoned-admins (Comprehensive PowerShell guide for new and seasoned admins)